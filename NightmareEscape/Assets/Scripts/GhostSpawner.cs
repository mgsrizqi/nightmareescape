﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GhostSpawner : MonoBehaviour {

	float timer;
	string lvl;
	bool isGhostKilled;
	GameObject playerGO;
	Player player;
	float killTimer;
	GameObject ghostGO;
	KeyCode keyToKill;
	float startFadeTime;
	Text keyToKillText;

	void Start () {
		EventManager.OnGhostKill += killGhost;
		timer = 0f;
		lvl = SceneManager.GetActiveScene().name;
		playerGO = GameObject.FindGameObjectWithTag("PlayerBody");
		player = playerGO.GetComponent<Player>();
		isGhostKilled = true;
		killTimer = 1000f;
		ghostGO = null;
		keyToKill = KeyCode.None;
		startFadeTime = 0;
		keyToKillText = GameObject.FindGameObjectWithTag("KeyToKill").GetComponent<Text>();
	}

	void Update () {
		timer += Time.deltaTime;
		killTimer -= Time.deltaTime;
		if(lvl == "InGameLvl1") {
			if(player.ghostTrigger != null) {
				GhostTrigger ghostTrigger = player.ghostTrigger.GetComponent<GhostTrigger>();
				ghostGO = ghostTrigger.ghostGO;
				killTimer = ghostTrigger.duration;
				if(ghostTrigger.ID == 0) {
					startFadeTime = Time.time;
				} else if(ghostTrigger.ID == 4) {
					startFadeTime = Time.time;
				}
				isGhostKilled = false;
				keyToKill = ghostTrigger.keyToKill;
				player.ghostTrigger = null;
			}
			if(ghostGO != null)
				ghostGO.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,Mathf.SmoothStep(0f, 1f, (Time.time - startFadeTime) / 2f));  
			if(killTimer > 0f && Input.GetKeyDown(keyToKill)) {
				EventManager.invokeGhostKill();
			}
			//spawnGhost(1, "a", 3f, 1.5f);
			//spawnGhost(2, "c", 6f, 1.5f);
		} else if(lvl == "InGameLvl2") {

		} else if(lvl == "InGameLvl3") {

		} else if(lvl == "InGameLvl4") {

		} else if(lvl == "InGameLvl5") {

		}
		Debug.Log(timer);
		if(keyToKill != KeyCode.None) {
			keyToKillText.gameObject.transform.parent.gameObject.SetActive(true);
			keyToKillText.text = keyToKill.ToString();
		} else {
			keyToKillText.gameObject.transform.parent.gameObject.SetActive(false);
			keyToKillText.text = " ";
		}
		if(killTimer < 0f && !isGhostKilled) {
			EventManager.invokeLose();
		}
	}
	/*
	void OnGUI()
	{
		GUI.contentColor = Color.white;
		if(keyToKill != KeyCode.None)
			GUI.Label(new Rect(600,300,100,100), keyToKill.ToString());
	}*/

	/*
	void spawnGhost(int ghostType, string key, float time, float duration) {
		GameObject ghost = null;
		if(ghostType == 1) {
			ghost = ghost1GO;
		} else if(ghostType == 2) {
			ghost = ghost2GO;
		} else if(ghostType == 3) {
			ghost = ghost3GO;
		}
		if(timer > time && timer < time + duration && !isSpawningGhost) {
			isSpawningGhost = true;
			isGhostKilled = false;
			ghost.SetActive(true);
		}
		if(Input.GetKeyDown(key) && isSpawningGhost) {
			EventManager.invokeGhostKill();
		}
		if(timer > time + duration && !isGhostKilled) {
			EventManager.invokeLose();
		}
		//Debug.Log(timer + " " + time + " " + duration);
	}
*/
	void killGhost() {
		Destroy(ghostGO);
		keyToKill = KeyCode.None;
		killTimer = 1000f;
		isGhostKilled = true;
	}

}
