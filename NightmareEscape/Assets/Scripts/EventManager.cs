﻿using UnityEngine;
using System.Collections;

public class EventManager : MonoBehaviour {
	
	public delegate void WinEvent();
	public static event WinEvent OnWin;
	public delegate void LoseEvent();
	public static event LoseEvent OnLose;
	public delegate void GhostKillEvent();
	public static event GhostKillEvent OnGhostKill;

	public static void invokeWin() {
		if(OnWin != null)
			OnWin();
	}

	public static void invokeLose() {
		if(OnLose != null)
			OnLose();
	}

	public static void invokeGhostKill() {
		if(OnGhostKill != null)
			OnGhostKill();
	}
}
