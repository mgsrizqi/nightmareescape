﻿using UnityEngine;
using System.Collections;

public class GhostTrigger : MonoBehaviour {

	public int ID;
	public GameObject ghostGO;
	public float duration;
	public KeyCode keyToKill;
	BoxCollider myCol;

	void Start () {
		ghostGO = Instantiate(ghostGO, transform.position, Quaternion.identity) as GameObject;
		myCol = GetComponent<BoxCollider>();
		if(ID == 0) {
			ghostGO.transform.Translate(new Vector3(0f, 1.3f, 7f * duration));
			ghostGO.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);
		} else if(ID == 1) {
			ghostGO.transform.Translate(new Vector3(0f, 1.5f, 7f * duration));
			ghostGO.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);
		} else if(ID == 2) {
			ghostGO.transform.Translate(new Vector3(0f, 1.5f, 7f * duration));
			ghostGO.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);
		} else if(ID == 4) {
			ghostGO.transform.Translate(new Vector3(0f, 1.5f, 7f * duration));
			ghostGO.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);
		} else if(ID == 5) {
			ghostGO.transform.Translate(new Vector3(0f, 1.5f, 7f * duration));
			ghostGO.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);
		} else if(ID == 6) {
			ghostGO.transform.Translate(new Vector3(0f, 1.5f, 7f * duration));
			ghostGO.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,0f);
		}
	}

	void Update() {
		if(ID == 5) {
			/*Vector3 ghostPos = ghostGO.transform.position;
			ghostPos = new Vector3(1f, ghostPos.y, ghostPos.z);
			ghostGO.transform.position = Vector3.Lerp(ghostGO.transform.position, ghostPos, Time.deltaTime);*/
		}
	}
}
