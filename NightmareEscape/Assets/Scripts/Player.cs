﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public GameObject ghostTrigger;

	void Start () {
		ghostTrigger = null;
	}

	void Update () {
	
	}

	void OnCollisionEnter(Collision col) {
		if(col.gameObject.name == "WinSphere") {
			EventManager.invokeWin();
		}
	}

	void OnTriggerExit(Collider col) {
		if(col.gameObject.tag == "GhostTrigger") {
			ghostTrigger = col.gameObject;
		}
	}
}
