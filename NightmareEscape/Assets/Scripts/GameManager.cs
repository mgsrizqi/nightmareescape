﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour {

	public static GameManager instance = null;
	string debug = "";
	GameObject gameOverText;
	float timer;

	void Awake() {
		if (instance == null) {
			instance = this;
		} else if (instance == this) {
			Destroy(gameObject);
		}
		DontDestroyOnLoad(gameObject);
	}

	public void Log(string msg)
	{
		debug = msg + "\n" + debug;
	}

	void OnGUI()
	{
		GUI.contentColor = Color.white;
		GUI.Label(new Rect(10,10,500,100), debug);
	}

	void Start() {
		EventManager.OnWin += gameWin;
		EventManager.OnLose += gameLose;
		gameOverText = GameObject.Find("GameOver");
		gameOverText.SetActive(false);
		timer = 0f;
	}

	void Update() {
		timer -= Time.deltaTime;
	}

	void gameWin() {
		SceneManager.LoadScene(0);
		Cursor.visible = true;
	}

	void gameLose() {
		gameOverText.SetActive(true);
		StartCoroutine(wait3Sec());
		SceneManager.LoadScene(0);
	}

	IEnumerator wait3Sec() {
		yield return new WaitForSeconds(3);
	}
}
