﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class FadingUI : MonoBehaviour {

	public GameObject story1;
	public GameObject[] menu;
	public GameObject button;

	private CanvasGroup can;
	private Text teksStory;

	public void StartFade(){
		can = GetComponent<CanvasGroup>();
		StartCoroutine(FadeOut());
		StartCoroutine(FadeInStory());
	}

	IEnumerator FadeOut(){
		while(can.alpha > 0){
			can.alpha -= (0.6f * Time.deltaTime);
			yield return null;
		}
		if(can.alpha <= 0){
			story1.SetActive(true);
			teksStory = story1.GetComponent<Text>();
			teksStory.color = new Color(1F, 1F, 1F, 0F);
			StopCoroutine(FadeOut());
		}

		yield return null;
	}

	IEnumerator FadeInStory(){
		teksStory = story1.GetComponent<Text>();
		while(teksStory.color.a <= 1){
			Color a = teksStory.color;
			a.a += (0.6f * Time.deltaTime);
			teksStory.color = a;
			yield return null;
		}

		if(menu[0].activeInHierarchy)
			for(int i = 0; i < menu.Length; i++)
				menu[i].SetActive(false);

		if(teksStory.color.a > 1){
			StopCoroutine(FadeInStory());
			StartCoroutine(FadeOutStory());
		}
		yield return null;
	}

	IEnumerator FadeOutStory(){
		teksStory = story1.GetComponent<Text>();
		while(teksStory.color.a >= 0){
			Color a = teksStory.color;
			a.a -= (0.3f * Time.deltaTime);
			teksStory.color = a;
			yield return null;
		}

		if(teksStory.color.a <= 0){
			button.SetActive(true);
			story1.SetActive(false);
			StopCoroutine(FadeOutStory());
		}
		yield return null;
	}

	public void QuitGame(){
		Application.Quit();
	}
}
