﻿using UnityEngine;
using System.Collections;

public class PropManager : MonoBehaviour {

	float timer;
	GameObject playerGO;
	GameObject canvasGO;
	GameObject sheIsHereGO;
	bool sheIsHereFlag;
	GameObject sheIsWatchingGO;
	bool sheIsWatchingFlag;
	GameObject wantToKillGO;
	bool wantToKillFlag;
	public GameObject sideKuntiGO;
	bool sideKuntiFlag;
	public GameObject ghoulKananGO;
	bool ghoulKananFlag;
	public GameObject ghoulKiriGO;
	bool ghoulKiriFlag;
	public GameObject werewolfGO;
	bool werewolfFlag;
	public GameObject shadowCuttedGO;
	bool shadowCuttedFlag;

	void Start () {
		timer = 0f;
		playerGO = GameObject.FindGameObjectWithTag("Player");
		canvasGO = GameObject.FindGameObjectWithTag("Canvas");
		sheIsHereGO = GameObject.Find("SheIsHere");
		sheIsHereGO.SetActive(false);
		sheIsWatchingGO = GameObject.Find("SheIsWatching");
		sheIsWatchingGO.SetActive(false);
		wantToKillGO = GameObject.Find("WantToKill");
		wantToKillGO.SetActive(false);
		sheIsHereFlag = true;
		sheIsWatchingFlag = true;
		wantToKillFlag = true;
		sideKuntiFlag = true;
		ghoulKananFlag = true;
		ghoulKiriFlag = true;
		werewolfFlag = true;
		shadowCuttedFlag = true;
	}

	void Update () {
		timer += Time.deltaTime;
		if(timer > 18f && timer < 21f) {
			if(sheIsHereFlag) {
				sheIsHereGO.SetActive(true);
				sheIsHereFlag = false;
			}
		} else {
			sheIsHereGO.SetActive(false);
		}
		if(timer > 23f) {
			if(sideKuntiFlag) {
				sideKuntiGO = Instantiate(sideKuntiGO, new Vector3(-6f, 1f, 150f), Quaternion.identity) as GameObject;
				sideKuntiFlag = false;
			}
			sideKuntiGO.transform.LookAt(playerGO.transform);
		}
		if(timer > 37f) {
			if(ghoulKananFlag) {
				ghoulKananGO = Instantiate(ghoulKananGO, new Vector3(6f, 1f, 235f), Quaternion.identity) as GameObject;
				ghoulKananFlag = false;
			}
			ghoulKananGO.transform.LookAt(playerGO.transform);
		}
		if(timer > 39f) {
			if(ghoulKiriFlag) {
				ghoulKiriGO = Instantiate(ghoulKiriGO, new Vector3(-6f, 1f, 245f), Quaternion.identity) as GameObject;
				ghoulKiriFlag = false;
			}
			ghoulKiriGO.transform.LookAt(playerGO.transform);
		}
		if(timer > 53f && timer < 56f) {
			if(sheIsWatchingFlag) {
				sheIsWatchingGO.SetActive(true);
				sheIsWatchingFlag = false;
			}
		} else {
			sheIsWatchingGO.SetActive(false);
		}
		if(timer > 67f && timer < 70f) {
			if(wantToKillFlag) {
				wantToKillGO.SetActive(true);
				wantToKillFlag = false;
			}
		} else {
			wantToKillGO.SetActive(false);
		}
		if(timer > 78f) {
			if(werewolfFlag) {
				werewolfGO = Instantiate(werewolfGO, new Vector3(6f, 1f, 490f), Quaternion.identity) as GameObject;
				werewolfFlag = false;
			}
			werewolfGO.transform.LookAt(playerGO.transform);
		}
		if(timer > 97f) {
			if(shadowCuttedFlag) {
				shadowCuttedGO = Instantiate(shadowCuttedGO, new Vector3(-6f, 1f, 600f), Quaternion.identity) as GameObject;
				shadowCuttedFlag = false;
			}
			shadowCuttedGO.transform.LookAt(playerGO.transform);
		}
	}
}
