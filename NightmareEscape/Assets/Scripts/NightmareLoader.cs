﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class NightmareLoader : MonoBehaviour {

	public GameObject platformGO;
	public GameObject endLightGO;
	string lvl;

	void Awake () {
		lvl = SceneManager.GetActiveScene().name;
		int maxPlatform = 1;
		if(lvl == "InGameLvl1") {
			maxPlatform = 33;
			for(int i = 0; i < maxPlatform; i++) {
				Instantiate(platformGO, new Vector3(0f, 0f , 30 * i), Quaternion.identity);
				if(i == maxPlatform-1) {
					Instantiate(endLightGO, new Vector3(0f, 2f , 30*i + 11), Quaternion.identity);
				}
			}
		} else if(lvl == "InGameLvl2") {

		} else if(lvl == "InGameLvl3") {

		} else if(lvl == "InGameLvl4") {

		} else if(lvl == "InGameLvl5") {

		}
	}
}
