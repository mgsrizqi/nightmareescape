﻿using UnityEngine;
using System.Collections;

public class RainFollow : MonoBehaviour {

	GameObject playerGO;

	void Start () {
		playerGO = GameObject.FindGameObjectWithTag("Player");
	}

	void Update () {
		Vector3 playerPos = playerGO.transform.position;
		playerPos = new Vector3(playerPos.x, 15.2f, playerPos.z);
		transform.position = Vector3.Lerp(transform.position, playerPos, Time.deltaTime * 2);
	}
}
