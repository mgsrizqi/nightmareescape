﻿using UnityEngine;
using System.Collections;

public class GameLoader : MonoBehaviour {

	public GameObject gameManager;

	// Use this for initialization
	void Awake () {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		if(GameManager.instance == null)
			Instantiate(gameManager);
	}
}
