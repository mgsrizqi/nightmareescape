﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Intro : MonoBehaviour {

	public Sprite[] introSprite;
	public bool play = false;
	public float transitionSpeed = 0.5f;
	public float timer = 1f;
	public float fadeTime = 0.2f;
	public GameObject canvas;
	public GameObject storyCanvas;
	public GameObject storyIndoor;
	public GameObject storyHall;
	public GameObject storyBfHall;
	public GameObject storyDoor;
	public GameObject buttonEnter;
	public GameObject end;

	private SpriteRenderer sr;
	private int indeks = 0;
	private bool fadeOut = true;
	private bool spriteTime = false;
	private bool playMenu = true;
	private bool gantiScene = false;
	private CanvasGroup can;
	private Text teksStoryIndoor;
	private Text teksStoryHall;


	// Use this for initialization
	void Start () {
		sr = GetComponent<SpriteRenderer>();
		can = canvas.GetComponent<CanvasGroup>();
	}

	void Update(){
		while(playMenu){
			float fade = 0.6f * Time.deltaTime;
			sr.color += new Color(fade, fade, fade, 0f);
			can.alpha += fade;

			if(sr.color.r > 1 && sr.color.g > 1 && sr.color.b > 1)
				playMenu = false;

			return;
		}
		while(!fadeOut && !spriteTime && play){
			if(indeks == 1){
				storyCanvas.SetActive(true);
				storyDoor.SetActive(true);
				timer -= Time.deltaTime;
				if(timer < 0f){
					storyDoor.SetActive(false);
					float fade = transitionSpeed * Time.deltaTime;
					sr.color += new Color(fade, fade, fade, 0f);

					if(sr.color.r > 1 && sr.color.g > 1 && sr.color.b > 1)
						fadeOut = true;
				}
			}
			else{
				float fade = transitionSpeed * Time.deltaTime;
				sr.color += new Color(fade, fade, fade, 0f);

				if(sr.color.r > 1 && sr.color.g > 1 && sr.color.b > 1)
					fadeOut = true;
			}

			return;
		}

		while(fadeOut && !spriteTime && play){
			if(indeks == 1){
				fadeOut = false;
				spriteTime = true;
			}
			else if(indeks == 5){
				StartCoroutine(IndoorAct());
				play = false;
			}
			else if(indeks == 7){
				StartCoroutine(HallAct());
				play = false;
			}

			float fade = transitionSpeed * Time.deltaTime;
			sr.color -= new Color(fade, fade, fade, 0f);

			if(sr.color.r < 0 && sr.color.g < 0 && sr.color.b < 0){
				fadeOut = false;
				spriteTime = true;
			}

			return;
		}

		if(spriteTime && play){
			if(indeks == 0){
				ChangeSprite();
				spriteTime = false;
			}
			else if(indeks < 4){
				timer -= Time.deltaTime;
				if(timer < 0f){
					ChangeSprite();
					timer = 1f;
					if(indeks == 4){
						timer = 5f;
					}
				}
			}
			else if(indeks == 6){
				storyCanvas.SetActive(true);
				storyBfHall.SetActive(true);
				timer -= Time.deltaTime;
				if(timer <= 0){
					storyBfHall.SetActive(false);
					sr.sprite = introSprite[5];
					transform.position = new Vector3(0f, 0f, 0f);
					spriteTime = false;
					indeks++;
					timer = 3.5f;
				}
			}
			else if(indeks == 8){
				//akhirintro
				timer -= Time.deltaTime;
				if(timer <= 0 && !gantiScene){
					end.SetActive(true);
					gantiScene = true;
				}
				else if(gantiScene){
					float fade = transitionSpeed * Time.deltaTime;
					can.alpha -= fade;

					if(can.alpha <= 0){
						//ganti scene
						SceneManager.LoadScene(1);
						Debug.Log("gantiscene");
					}
				}
			}
			else 
				ChangeSprite();
		}
	}

	void ChangeSprite(){
		if(indeks < introSprite.Length) {
			sr.sprite = introSprite[indeks];
			indeks++;
			if(indeks > 3) {
				spriteTime = false;
			}
		}
	}

	IEnumerator IndoorAct(){
		storyCanvas.SetActive(true);
		storyIndoor.SetActive(true);
		StartCoroutine(FadeInStory());
		yield return null;
	}

	public void Begin(){
		play = true;
	}
		
	IEnumerator HallAct(){
		storyCanvas.SetActive(true);
		storyHall.SetActive(true);
		StartCoroutine(HallStory());
		yield return null;
	}

	IEnumerator HallStory(){
		teksStoryHall = storyHall.GetComponent<Text>();
		while(teksStoryHall.color.a <= 1){
			Color a = teksStoryHall.color;
			a.a += (fadeTime * Time.deltaTime);
			teksStoryHall.color = a;
			yield return null;
		}
			
		if(teksStoryHall.color.a > 1){
			StopCoroutine(HallStory());
			StartCoroutine(HallFade());
		}

		yield return null;
	}

	IEnumerator HallFade(){
		teksStoryHall = storyHall.GetComponent<Text>();
		while(teksStoryHall.color.a >= 0){
			Color a = teksStoryHall.color;
			a.a -= (fadeTime * Time.deltaTime);
			teksStoryHall.color = a;
			yield return null;
		}

		play = true;
		indeks++;
		storyHall.SetActive(false);
		storyCanvas.SetActive(false);
		StopCoroutine(HallAct());
		StopCoroutine(HallFade());
	}

	IEnumerator FadeInStory(){
		teksStoryIndoor = storyIndoor.GetComponent<Text>();
		while(teksStoryIndoor.color.a <= 1){
			Color a = teksStoryIndoor.color;
			a.a += (fadeTime * Time.deltaTime);
			teksStoryIndoor.color = a;
			yield return null;
		}

		if(teksStoryIndoor.color.a > 1){
			StopCoroutine(FadeInStory());
			StartCoroutine(FadeOutStory());
		}
		yield return null;
	}

	IEnumerator FadeOutStory(){
		teksStoryIndoor = storyIndoor.GetComponent<Text>();
		while(teksStoryIndoor.color.a >= 0){
			Color a = teksStoryIndoor.color;
			a.a -= (fadeTime * Time.deltaTime);
			teksStoryIndoor.color = a;
			yield return null;
		}

		if(teksStoryIndoor.color.a <= 0){
			can.alpha = 1;
			Vector3 end = new Vector3(0f, -0.2f, -3f);
			while(transform.position != end){
				transform.position = Vector3.MoveTowards(transform.position, end, Time.deltaTime);
				yield return null;
			}

			storyIndoor.SetActive(false);
			storyCanvas.SetActive(false);
			buttonEnter.SetActive(true);
			indeks++;
			StopCoroutine(IndoorAct());
			StopCoroutine(FadeOutStory());
		}
	}
}
